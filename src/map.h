#pragma once

#include <iostream>
#include "room.h"
#include "utils.h"
#include "tiles.h"

class Map{
public:
	Map(const int width, const int height);
	~Map();

	int **world;

	int width, height;

	void Create();
	bool Update(int px, int py);

	void LocatePlayerStart(int* playerPosX, int* playerPosY);

	void SetExitLocation();

	
};