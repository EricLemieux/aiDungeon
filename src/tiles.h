#pragma once

#define	STONE 0
#define	EMPTY 1
#define	AIPLAYER 2
#define	USERPLAYER 3
#define EXIT 4
#define UNKNOWN 99

int GetTileValue(int type);

struct Tile{
	int idX;
	int idY;
	int cost;
	int totalCost;
	int prevX;
	int prevY;
};