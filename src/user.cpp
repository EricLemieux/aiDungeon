#include "user.h"

void User::Update(int dx, int dy, int** fullWorld){
	if(playerPosY+dy >= 0 && playerPosY+dy < height && 
		playerPosX+dx >= 0 && playerPosX+dx < width && 
		this->world[playerPosY+dy][playerPosX+dx] != STONE){
		
		playerPosX += dx;
		playerPosY += dy;
		int viewDist = 3;

		for(int i = playerPosY-viewDist+1; i < playerPosY+viewDist; ++i){
			for(int j = playerPosX-viewDist+1; j < playerPosX+viewDist; ++j){
				if(i >= 0 && i < height && j >= 0 && j < width){
					this->world[i][j] = fullWorld[i][j];
				}
			}
		}
	}
}