#include "map.h"

Map::Map(const int width, const int height){
	this->width = width;
	this->height = height;

	this->world = new int*[height];
	for(int i = 0; i < height; ++i){
		this->world[i] = new int[width]{0};
	}

	Create();
	Create();
	Create();
	Create();

	SetExitLocation();
}

Map::~Map(){
	delete[] this->world;
}

void Map::Create(){
	int maxNumRooms = RandInt(width*2,width*5);
	int currentRoom = 0;
	Room *rooms = new Room[maxNumRooms];

	Room r = Room(width/2,height/2, RandInt(1,3), RandInt(1,3));
	rooms[0] = r;
	currentRoom++;

	while(currentRoom < maxNumRooms){
		//Gen room
		Room r = Room(RandInt(rooms[currentRoom-1].x - RandInt(1,3), rooms[currentRoom-1].x + RandInt(2,4)), RandInt(rooms[currentRoom-1].y - RandInt(1,3), rooms[currentRoom-1].y + RandInt(2,4)), RandInt(2,4), RandInt(2,4));
		rooms[currentRoom] = r;

		for(int i = r.y-r.height; i < r.y+r.height; ++i){
			for(int j = r.x-r.width; j < r.x+r.width; ++j){
				if(i >= 0 && i < this->height && j >= 0 && j < this->width){
					this->world[i][j] = EMPTY;
				}
			}
		}
		currentRoom++;
	}
}

bool Map::Update(int px, int py){
	if(world[py][px] == EXIT)
		return true;
	return false;
}

void Map::LocatePlayerStart(int* playerPosX, int* playerPosY){
	//Place the player at the start
	for(int i = 0 ; i < height; ++i){
		for(int j = 0; j < width; ++j){
			if(this->world[i][j] == EMPTY){
				*playerPosX = j;
				*playerPosY = i;
				return;
			}
		}
	}
}

void Map::SetExitLocation(){
	int i,j;
	do{
		i = rand()%height;
		j = rand()%width;
	}while(world[i][j] != EMPTY);
	world[i][j] = EXIT;
}