#pragma once

#include "tiles.h"

class Player{
public:
	Player(const int width, const int height);
	//~Player(){delete[] this->world;}

	int **world;

	int width, height;

	int playerPosX, playerPosY;

	void Create();
	virtual bool Update(){return true;}
};