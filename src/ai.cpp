#include "ai.h"
#include <iostream>

void AI::Update(int** fullWorld, bool canMove){
	const int viewDist = 3;
	for(int i = playerPosY-viewDist+1; i < playerPosY+viewDist; ++i){
		for(int j = playerPosX-viewDist+1; j < playerPosX+viewDist; ++j){
			if(i >= 0 && i < height && j >= 0 && j < width){
				this->world[i][j] = fullWorld[i][j];
			}
		}
	}

	//If we are just updating the view of the ai we dont want it to move as well so return here
	if(!canMove){return;}

	Tile cp,dp;
	cp.idX = playerPosX;
	cp.idY = playerPosY;

	//Figure out where the hell we want to go, decision trees up in this bitch
	targetQuad = SelectBestQuadrent();

	//Chose the best tile inside that quadrent
	dp = SelectBestTileFromQuadrent(targetQuad);
	targetX = dp.idX;
	targetY = dp.idY;

	//Decide the best way of getting there, ie do some A* here
	Tile nextMove = Move(cp, dp);//SelectNextMove(cp, dp);

	//Lets go
	playerPosX = nextMove.idX;
	playerPosY = nextMove.idY;
}

Tile AI::Move(Tile start, Tile end){
	//Find the distance to the tile we want
	int dx, dy;
	dx = end.idX - start.idX;
	dy = end.idY - start.idY;

	//Normalize it
	int ndx = dx==0?0:dx/abs(dx);
	int ndy = dy==0?0:dy/abs(dy);

	//Make the new shitty tile
	Tile temp;
	temp.idX = start.idX + ndx;
	temp.idY = start.idY + ndy;

	if(world[temp.idY][temp.idX] == STONE){
		failCount[targetQuad.idY][targetQuad.idX]++;
		for(int t = 0; t < 4; ++t){
			int i = rand()%4;

			if(i == 0){
				temp.idX = start.idX + 1;
				temp.idY = start.idY + 0;
			}else if(i == 1){
				temp.idX = start.idX + 0;
				temp.idY = start.idY + 1;
			}else if(i == 2){
				temp.idX = start.idX - 1;
				temp.idY = start.idY + 0;
			}else{
				temp.idX = start.idX + 0;
				temp.idY = start.idY - 1;
			}

			if(temp.idX < width && temp.idX >= 0 && temp.idY < height && temp.idY >= 0 && world[temp.idY][temp.idX] != STONE){
				break;
			}
		}
	}	

	//Return it
	return temp;
}

Tile AI::SelectBestTileFromQuadrent(Quadrent destinationQuadrent){
	std::vector<Tile> bestTiles;
	int bestValue = -9999;
	for(int i = destinationQuadrent.idY * heightStride; i < (destinationQuadrent.idY * heightStride) + heightStride; ++i){
		for(int j = destinationQuadrent.idX * widthStride; j < (destinationQuadrent.idX * widthStride) + widthStride; ++j){
			int cv = GetTileValue(world[i][j]);
			if(cv > bestValue){
				bestValue = cv;
				bestTiles.clear();
				Tile c;
				c.idY = i;
				c.idX = j;
				bestTiles.push_back(c);
			}else if(cv == bestValue){
				Tile c;
				c.idY = i;
				c.idX = j;
				bestTiles.push_back(c);
			}
		}
	}
	int choice = 0;
	if(bestTiles.size() > 1){choice = rand()%bestTiles.size();}
	return bestTiles[choice];
}

Quadrent AI::SelectBestQuadrent(){
	//First we going to split up the map into smaller quadrents so we can decide the general area we want to go
	std::vector<Quadrent> bestQuadrents;
	Quadrent q;
	q.value = -9999;
	bestQuadrents.push_back(q);

	const int playerQuadrentX = int(playerPosX/widthStride);
	const int playerQuadrentY = int(playerPosY/heightStride);

	for(int i = 0; i < heightQuadrents; ++i){
		for(int j = 0; j < widthQuadrents; ++j){
			//Determine the value of this quadrent
			Quadrent currentQ;
			currentQ.value = 0;
			currentQ.idY = i;
			currentQ.idX = j;

			//start the current value with the distance to the other quadrent
			int xdist = playerQuadrentX - j;
			int ydist = playerQuadrentY - i;
			if(xdist<0){xdist *= -1;}
			if(ydist<0){ydist *= -1;}

			currentQ.value += 10 - xdist*2 - ydist*2;

			currentQ.value -= failCount[i][j]*5;

			for(int k = i * heightStride; k < (i * heightStride) + heightStride; ++k){
				for(int l = j * widthStride; l < (j * widthStride) + widthStride; ++l){
					//Get the value of the individual tile and add it to the quadrents value
					currentQ.value += GetTileValue(world[k][l]);
				}
			}

			if(currentQ.value > bestQuadrents[0].value){
				//This quad is the best so far
				bestQuadrents.clear();
				bestQuadrents.push_back(currentQ);
			}else if(currentQ.value == bestQuadrents[0].value){	
				//This quad is just as good as the rest
				bestQuadrents.push_back(currentQ);
			}

		}
	}

	//If there is a tie between multiple quadrents pick one at random
	int choice = 0;
	if(bestQuadrents.size() > 1){choice = rand()%bestQuadrents.size();}
	
	return bestQuadrents[choice];
	
}