#include "player.h"

Player::Player(const int width, const int height){
	this->width = width;
	this->height = height;

	this->world = new int*[height];
	for(int i = 0; i < height; ++i){
		this->world[i] = new int[width]{0};
	}

	Create();
}

void Player::Create(){
	for(int i = 0 ; i < height; ++i){
		for(int j = 0; j < width; ++j){
			this->world[i][j] = 99;
		}
	}
}