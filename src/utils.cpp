#include "utils.h"

int RandInt(const int min, const int max){
	return rand() % (max-min) + min;
}

template< class T > bool BoxBoxCollision(T b1x, T b1y, T b1w, T b1h, T b2x, T b2y, T b2w, T b2h){
	if(b1x+b1w < b2x || b1x > b2x+b2w)
		return false;
	if(b1y+b1h < b2y || b1y > b2y+b2h)
		return false;
	return true;
}