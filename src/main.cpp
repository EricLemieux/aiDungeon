#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "map.h"
#include "ai.h"
#include "tiles.h"
#include "user.h"
#include <curses.h>

void DrawWorld(int** world, const int width, const int height, int userX, int userY, int aiX, int aiY);

int main(int argc, char **argv){
	int width = 50;
	int height = 50;

	srand(time(NULL));

	for(int i = 0; i < argc; ++i){
		//Get some info about the size of the map here
		if(!strcmp("width",argv[i])){
			sscanf(argv[i+1],"%i",&width);
		}
		if(!strcmp("height",argv[i])){
			sscanf(argv[i+1],"%i",&height);
		}
		if(!strcmp("seed",argv[i])){
			int seed;
			sscanf(argv[i+1],"%i",&seed);
			srand(seed);
		}
	}

	//Create the map here
	Map map = Map(width, height);
	AI ai = AI(width, height);
	User user = User(width, height);

	map.LocatePlayerStart(&user.playerPosX, &user.playerPosY);
	map.LocatePlayerStart(&ai.playerPosX, &ai.playerPosY);

	initscr();
	cbreak();
	refresh();

	int **currentMap = user.world;


	user.Update(0,0, map.world);
	ai.Update(map.world, false);

	//Start the game loop
	bool mapSolved = false;
	while(!mapSolved){
		if(map.Update(ai.playerPosX, ai.playerPosY)){
			clear(); 
			refresh();
			std::cout<<"AI player wins the game!\r\n";
			break;
		}else if(map.Update(user.playerPosX, user.playerPosY)){
			clear(); 
			refresh();
			std::cout<<"User wins the game!\r\n";
			break;
		}

		//Render the map to the console
		DrawWorld(currentMap,width,height, user.playerPosX, user.playerPosY, ai.playerPosX, ai.playerPosY);

		char c = ' ';
		bool waitingForInput = true;
		while(waitingForInput){
			switch(getch()){
				case '\033':
					getch();
			    	switch(getch()) {
				        case 'A':
				        	//Up
				            waitingForInput = false;
							user.Update(0,-1, map.world);
				            break;
				        case 'B':
				        	//Down-
				            waitingForInput = false;
							user.Update(0,1, map.world);
				            break;
				        case 'C':
				        	//Right
				            waitingForInput = false;
							user.Update(1,0, map.world);
				            break;
				        case 'D':
				        	//Left
				            waitingForInput = false;
							user.Update(-1,0, map.world);
				            break;
				    }
					break;
				case 's':
					std::cout<<"SSSSSSSSSSsssssnake\r\n";
					break;
				case char('1'):
					currentMap = user.world;
					DrawWorld(currentMap,width,height, user.playerPosX, user.playerPosY, ai.playerPosX, ai.playerPosY);
					break;
				case char('2'):
					currentMap = ai.world;
					DrawWorld(currentMap,width,height, user.playerPosX, user.playerPosY, ai.playerPosX, ai.playerPosY);
					break;
				case char('3'):
					currentMap = map.world;
					DrawWorld(currentMap,width,height, user.playerPosX, user.playerPosY, ai.playerPosX, ai.playerPosY);
					break;
				default:
					break;
			}
		}

		ai.Update(map.world);		
	}

	char c;
	std::cin>>c;

	//exit the game loop and store some data
	endwin();

	return 0;
}

void DrawWorld(int** world, const int width, const int height, int userX, int userY, int aiX, int aiY){
	clear(); 
	refresh();
	for (int i = 0; i < height; ++i){
		for(int j = 0; j < width; ++j){
			

			if(i == userY && j == userX && world[i][j] == EMPTY){
				std::cout<<"\e[42m ";
			}else if(i == aiY && j == aiX && world[i][j] == EMPTY){
				std::cout<<"\e[41m ";
			}else{
				switch(world[i][j]){
					case STONE:
						std::cout<<"\e[100m ";
						break;
					case EMPTY:
						std::cout<<"\e[40m ";
						break;
					case EXIT:
						std::cout<<"\e[43m ";
						break;
					default:
						std::cout<<"\e[107m ";
						break;
				}
			}
		}
		std::cout<<"\e[49m\r"<<std::endl;
	}	
}